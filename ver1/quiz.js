Vue.use(VueLocalStorage);

function shuffle(array) {
    // Shuffle array items in place
    var currentIndex = array.length,
        temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

// Vue.component('menu-level', {
//     props: ['levels', 'selectedLevel1', 'selectedLevel2', 'selectedLevel3'],
//     template: '#menu-level-template',
//     // data: function() {
//     //     return {
//     //         selectedLevel1: {id: 0},
//     //         selectedLevel2: {id: 0},
//     //         selectedLevel3: {id: 0}
//     //     };
//     // },
//     computed: {
//         possibleLevel2s: function() {
//             var self = this;
//             return this.levels.level2s.filter(function(level2) {
//                 return level2.level1_id === self.selectedLevel1.id;
//             });
//         },
//         possibleLevel3s: function() {
//             var self = this;
//             return this.levels.level3s.filter(function(level3) {
//                 return level3.level2_id === self.selectedLevel2.id;
//             });
//         }

//     }

// });

var app = new Vue({
    el: '#app',
    data: {
        QUESTIONS_PER_QUIZ: 5,
        POINTS_PER_QUESTION: 100,

        levels: { level1s: [], level2s: [], level3s: []},
        questions: [{text:''}],
        currentSelectedAnswer: null,
        currentQuestionIndex: -1,
        finishedQuestionCount: 0,
        wrongAnswerIndexes: [],

        selectedLevel1: {id: 0},
        selectedLevel2: {id: 0},
        selectedLevel3: {id: 0},

        audios: {
            correct_audios: [
                new Audio('audio/correct.mp3'),
                new Audio('audio/good.mp3'),
                new Audio('audio/right.mp3'),
                new Audio('audio/thatsit.mp3'),
                new Audio('audio/yes.mp3'),
                new Audio('audio/yessa.mp3'),
                new Audio('audio/youarecorrect.mp3')
            ],
            wrong_audios: [
                new Audio('audio/no.mp3')
            ]
        }
    },
    mounted: function() {
        //Get data from persistence storage

        this.levels = Vue.ls.get('levels', this.levels);
        this.questions = Vue.ls.get('questions', this.questions);
        
    },
    methods: {
        start: function() {
            this.filterQuestions();
            this.currentQuestionIndex = 0;
        },
        filterQuestions: function() {
            var self = this;
            if (this.selectedLevel1 && this.selectedLevel1.id)
                this.questions = this.questions.filter(function(question) { return question.level1_id === self.selectedLevel1.id; });
            if (this.selectedLevel2 && this.selectedLevel2.id)
                this.questions = this.questions.filter(function(question) { return question.level2_id === self.selectedLevel2.id; });
            if (this.selectedLevel3 && this.selectedLevel3.id)
                this.questions = this.questions.filter(function(question) { return question.level3_id === self.selectedLevel3.id; });

            this.questions.splice(this.QUESTIONS_PER_QUIZ);
            shuffle(this.questions);
        },

        selectChoice: function(choice, event) {
            this.currentSelectedAnswer = choice;

            if (this.finishedQuestionCount < this.questions.length) {
                if (this.currentCorrectAnswer !== this.currentSelectedAnswer) {
                    this.wrongAnswerIndexes.push(this.currentQuestionIndex);
                    var wrong_audio = this.audios.wrong_audios[Math.floor(Math.random() * this.audios.wrong_audios.length)];
                    wrong_audio.play();
                }
                else {
                    var correct_audio = this.audios.correct_audios[Math.floor(Math.random() * this.audios.correct_audios.length)];
                    correct_audio.play();

                    // confetti();

                }
                setTimeout(this.nextQuestion, 200);
            }
        },
        nextQuestion: function() {
            if (this.currentQuestionIndex + 1 < this.questions.length)
                this.currentQuestionIndex++;
            // this.currentQuestionIndex = this.questions.length === this.currentQuestionIndex + 1 ? this.currentQuestionIndex : this.currentQuestionIndex + 1;
            this.finishedQuestionCount++;
        },

        /* ANIMATION */
        pointsAnimeEnter: function(points) {
            var pointsAnime = anime({
                targets: "input.points",
                value: points,
                round: 1,
                easing: 'easeInOutExpo',
                autoplay: true
            });
        }
    },
    watch: {
        points: function(newValue, oldValue) {
            // this.pointsAnimeEnter(newValue);
        }
    },
    computed: {
        choices: function() {
            question = this.questions[this.currentQuestionIndex];
            var order = shuffle([1,2,3,4]);
            return new Array(question["choice" + order[0]], question["choice" + order[1]], question["choice" + order[2]], question["choice" + order[3]]);
        },
        points: function() {
            return (this.finishedQuestionCount - this.wrongAnswerIndexes.length) * this.POINTS_PER_QUESTION;
        },
        currentQuestionText: function() {
            return this.questions[this.currentQuestionIndex].text;
        },
        currentCorrectAnswer: function() {
            return this.questions[this.currentQuestionIndex].choice1;
        },
        possibleLevel2s: function() {
            var self = this;
            return this.levels.level2s.filter(function(level2) {
                return level2.level1_id === self.selectedLevel1.id;
            });
        },
        possibleLevel3s: function() {
            var self = this;
            return this.levels.level3s.filter(function(level3) {
                return level3.level2_id === self.selectedLevel2.id;
            });
        }
    }
});