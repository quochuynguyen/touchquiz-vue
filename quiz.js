Vue.use(VueLocalStorage);

function shuffle(array) {
    // Shuffle array items in place
    var currentIndex = array.length,
        temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}


var app = new Vue({
    el: '#app',
    data: {
        QUESTIONS_PER_QUIZ: 5,
        POINTS_PER_QUESTION: 100,

        levels: Vue.ls.get('levels', this.levels),//{ level1s: [], level2s: [], level3s: []},
        questions: Vue.ls.get('questions', this.questions),//[{text:''}],
        currentSelectedAnswer: null,
        currentQuestionIndex: 0,
        finishedQuestionCount: 0,
        wrongAnswerIndexes: [],
        popup: false,
        popupTimeoutID: null,
        showQuestion: true,

        selectedLevel1: {id: 0},
        selectedLevel2: {id: 0},
        selectedLevel3: {id: 0},

        selectedAnswerIndex: 0,

        // Reset the game if no click in 10mins
        timeoutLimit: 10*60*1000,
        // timeout: setTimeout("location.reload(true);", 10*60*1000),

        audios: {
            correct_audios: [
                new Audio('audio/correct.mp3'),
                new Audio('audio/good.mp3'),
                new Audio('audio/right.mp3'),
                new Audio('audio/thatsit.mp3'),
                new Audio('audio/yes.mp3'),
                new Audio('audio/yessa.mp3'),
                new Audio('audio/youarecorrect.mp3')
            ],
            wrong_audios: [
                new Audio('audio/slot-sound-1.mp3')
            ],
            running_point_audio: new Audio('audio/jackpot-win.mp3')
        }
    },
    mounted: function() {
        //Get data from persistence storage
        // this.levels = Vue.ls.get('levels', this.levels);
        // this.questions = Vue.ls.get('questions', this.questions);

        // confetti();

        // Loop audio
        var background_theme = new Audio('audio/littlebigplanet-thegardens.mp3');
        background_theme.volume = 0.5;
        background_theme.addEventListener('ended', function() {
            this.currentTime = 0;
            this.play();
        }, false);
        background_theme.play();

    },
    methods: {
        start: function() {
            // this.filterQuestions();
            this.currentQuestionIndex = 0;
            this.transition(".home-page", ".game-page", 18);
        },
        filterQuestions: function() {
            var self = this;
            this.questions = Vue.ls.get('questions', this.questions);
            if (this.selectedLevel1 && this.selectedLevel1.id)
                this.questions = this.questions.filter(function(question) { return question.level1_id === self.selectedLevel1.id; });
            if (this.selectedLevel2 && this.selectedLevel2.id)
                this.questions = this.questions.filter(function(question) { return question.level2_id === self.selectedLevel2.id; });
            if (this.selectedLevel3 && this.selectedLevel3.id)
                this.questions = this.questions.filter(function(question) { return question.level3_id === self.selectedLevel3.id; });

            this.questions.splice(this.QUESTIONS_PER_QUIZ);
            shuffle(this.questions);
        },
        selectChoice: function(choice, event) {
            var self = this;
            this.currentSelectedAnswer = choice;

            if (this.finishedQuestionCount < this.questions.length) {
                this.finishedQuestionCount++;

                if (this.currentCorrectAnswer !== this.currentSelectedAnswer) {
                    this.wrongAnswerIndexes.push(this.currentQuestionIndex);
                    // Play audio
                    var wrong_audio = this.audios.wrong_audios[Math.floor(Math.random() * this.audios.wrong_audios.length)];
                    wrong_audio.play();
                    
                    ['#choice0', '#choice1', '#choice2', '#choice3'].map(function(elRef) {
                        self.addAnimation(elRef);
                    });
                    setTimeout(this.nextQuestion, 500);
                    this.openPopup();
                    
                }
                else {
                    var correct_audio = this.audios.correct_audios[Math.floor(Math.random() * this.audios.correct_audios.length)];
                    correct_audio.play();

                    this.audios.running_point_audio.play();
                    // this.addAnimation('#points');
                    this.runningPointAnime(this.points);
                    this.nextQuestion();
                }
                
                
            }
        },

        nextQuestion: function() {
            if (this.currentQuestionIndex + 1 < this.questions.length)
                this.currentQuestionIndex++;
            
            // this.transition(".game-page", ".game-page", 18);
        },



        /* GUI & ANIMATION */
        resetTimeout: function() {
            clearTimeout(this.timeout);
            this.timeout = setTimeout("location.reload(true);", this.timeoutLimit);
        },
        openPopup: function() {
            var self = this;
            this.$el.querySelector('.img-section img').setAttribute("src", "images/wrong-" + (Math.floor(Math.random() * 5) + 1) + ".gif");
            this.transition(".game-page", ".wrong-image-page", 49);
            setTimeout(function() {
                self.popup = true;
                self.transition(".wrong-image-page", ".popup-page", 2);
                self.addAnimation('#line');
                self.popupTimeoutID = setTimeout(self.closePopup, 3100);
            }, 2000);
        },
        closePopup: function() {
            // debugger;
            if (this.popup) {
                this.popup = false;
                this.transition(".popup-page", ".game-page", 3);
            }
        },
        cancelPopupTimeout: function() {
            clearTimeout(this.popupTimeoutID);
            this.removeClass('#line', 'animate');
        },
        toScoreBoard: function() {
            this.transition(".game-page", ".score-board-page", 3);
        },
        removeClass: function(elRef, classname) {
            if (this.$el.querySelector(elRef) !== undefined)
                this.$el.querySelector(elRef).classList.remove(classname);
        },
        addAnimation: function(elRef) {
            var element = this.$el.querySelector(elRef);
            element.classList.add('animate');
            element.addEventListener("animationend", function fn(){
                element.removeEventListener("animationend", fn);
                element.classList.remove('animate');
            });
        },
        transition: function(curPageRef, nextPageRef, animationNumber) {
            // Animation to transition from one page to another
            var curPage = this.$el.querySelector(curPageRef);
            var nextPage = this.$el.querySelector(nextPageRef);

            // if (curPageRef === nextPageRef) {
            //     nextPage = curPage.clone();
            // }
            // debugger;
            animation = getAnimation(animationNumber);
            var outClass = animation.outClass, inClass = animation.inClass;

            outClass.split(" ").map(function(cla) { curPage.classList.add(cla); });
            curPage.addEventListener("animationend", function fn() {
                curPage.removeEventListener("animationend", fn);
                outClass.split(" ").map(function(cla) { curPage.classList.remove(cla); });
                curPage.classList.remove('display-on');
            });

            nextPage.classList.add('display-on');
            inClass.split(" ").map(function(cla) { nextPage.classList.add(cla); });
            nextPage.addEventListener("animationend", function afn() {
                nextPage.removeEventListener("animationend", afn);
                inClass.split(" ").map(function(cla) { nextPage.classList.remove(cla); });

            });
        },
        runningPointAnime: function(newValue) {
            var domAttributes = anime({
                targets: '.points-number',
                innerHTML: newValue,
                round: 1,
                easing: 'easeInOutExpo',
                duration: 1800
            });
        }
    },
    watch: {
        // Flip on and off #question so animation takes effect
        currentQuestionText: function(newValue, oldValue) {
            var self = this;
            this.showQuestion = false;
            setTimeout(function() {
                self.showQuestion = true;
            }, 200);
        }
    },
    computed: {
        choices: function() {
            question = this.questions[this.currentQuestionIndex];
            var order = shuffle([1,2,3,4]);
            return new Array(question["choice" + order[0]], question["choice" + order[1]], question["choice" + order[2]], question["choice" + order[3]]);
        },
        points: function() {
            return (this.finishedQuestionCount - this.wrongAnswerIndexes.length) * this.POINTS_PER_QUESTION;
        },
        currentQuestionText: function() {
            return this.questions[this.currentQuestionIndex].text;
        },
        currentCorrectAnswer: function() {
            return this.questions[this.currentQuestionIndex].choice1;
        },
        possibleLevel2s: function() {
            var self = this;
            return this.levels.level2s.filter(function(level2) {
                return level2.level1_id === self.selectedLevel1.id;
            });
        },
        possibleLevel3s: function() {
            var self = this;
            return this.levels.level3s.filter(function(level3) {
                return level3.level2_id === self.selectedLevel2.id;
            });
        }
    }
});