Vue.use(VueLocalStorage);

Vue.component('question-item', {
    props: ['question', 'levels'],
    template: '#question-view-template',
    computed: {
        // Get level obj based on level id
        level1: function() {
            var self = this;
            return this.levels.level1s.filter(function(level1) {
                return level1.id === self.question.level1_id;
            })[0];
        },
        level2: function() {
            var self = this;
            return this.levels.level2s.filter(function(level2) {
                return level2.id === self.question.level2_id;
            })[0];
        },
        level3: function() {
            var self = this;
            return this.levels.level3s.filter(function(level3) {
                return level3.id === self.question.level3_id;
            })[0];
        }
    }
});

Vue.component('question-edit', {
    props: ['question', 'levels'],
    template: '#question-edit-template',
    computed: {
        // Get all sub-levels based on a parent level (level1 > level2 > level3)
        possibleLevel2s: function() {
            var self = this;
            return this.levels.level2s.filter(function(level2) {
                return level2.level1_id === self.question.level1_id;
            });
        },
        possibleLevel3s: function() {
            var self = this;
            return this.levels.level3s.filter(function(level3) {
                return level3.level2_id === self.question.level2_id;
            });
        }
    }
});

var app = new Vue({
    el: '#app',
    data: {
        levels: {
            level1s: [
                {id: 1, name:'Math', code: 'M'},
                {id: 2, name:'Physics', code: 'P'}
            ],
            level2s: [
                {id: 11, level1_id: 1, name:'Chapter 1', code: 'C1'},
                {id: 12, level1_id: 1, name:'Chapter 2', code: 'C2'},
                {id: 13, level1_id: 2, name:'Section A', code: 'SA'}
            ],
            level3s: [
                {id: 31, level2_id: 11, name:'Easy', code: 'E'},
                {id: 32, level2_id: 12, name:'Hard', code: 'H'},
                {id: 33, level2_id: 13, name:'A', code: 'A'},
                {id: 34, level2_id: 13, name:'B', code: 'B'}
            ]
        },
        questions: [
            {level1_id: 1, level2_id: 11, level3_id: 31, text: 'What type of number is Pi?', choice1: 'Irrational', choice2: 'Rational', choice3: 'Natural', choice4: 'Negative'},
            {level1_id: 1, level2_id: 11, level3_id: 31, text: 'What is the least commom denominator of 3 and 9?', choice1: '9', choice2: '27', choice3: '3', choice4: '6'},
            {level1_id: 1, level2_id: 12, level3_id: 32, text: 'What is square root of 4?', choice1: '2', choice2: '4', choice3: '1', choice4: '0'},
            {level1_id: 2, level2_id: 13, level3_id: 33, text: 'How old is Earth?', choice1: '4 billion years', choice2: '2000 years', choice3: '20 days', choice4: '100 days'},
            {level1_id: 2, level2_id: 13, level3_id: 34, text: 'How long does it take for light to travel from Sun to Earth?', choice1: '8 mins', choice2: '8 hours', choice3: '1 year', choice4: '10 days'}
        ],
        
        editingQuestion: {level1_id: null, level2_id: null, level3_id: null, text: '', choice1: '', choice2: '', choice3: '', choice4: ''}, //question that being edit
        isEditing: false, //2 mutually exclusive UI modes: Edit or Add
        selectedQuestion: null //current selected question on the list
    },
    mounted: function() {
        //Get data from persistence storage
        this.questions = Vue.ls.get('questions', this.questions);
        this.levels = Vue.ls.get('levels', this.levels);
        //Save data before leaving
        window.addEventListener('beforeunload', this.persist);
    },
    methods: {
        selectToEdit: function(question, event) {
            this.isEditing = true;
            this.selectedQuestion = question;
            this.editingQuestion = question;
        },
        save: function(event) {
            if (this.editingQuestion.text !== '') {
                this.questions.push(this.editingQuestion);
                this.selectedQuestion = this.editingQuestion;
                this.isEditing = true;
            }
        },
        persist: function() {
            //Persistence storage
            Vue.ls.set('questions', this.questions);
            Vue.ls.set('levels', this.levels);
        },
        add: function(event) {
            this.editingQuestion = {level1_id: null, level2_id: null, level3_id: null, text: '', choice1: '', choice2: '', choice3: '', choice4: ''};
            this.selectedQuestion = this.editingQuestion;
            this.isEditing = false;
        }
    }
});

